const express = require('express');

const mongoose = require ('mongoose');

const app = express();

const port = 3001;

// MongoDB Connection

mongoose.connect("mongodb+srv://admin:admin123@batch204-hufano.2rq00td.mongodb.net/User?retryWrites=true&w=majority", 
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

// Connection to the database
let db = mongoose.connection;

//If a connectin error occured, output in the console
db.on("error", console.error.bind(console, "Connection Error"));

//If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database."));

// Mongoose Schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

// Model 
const User = mongoose.model("User", userSchema);

// Middleware
app.use(express.json());


// ROUTE to create a User

app.post('/signup', (request, response) => {

	User.findOne({username: request.body.username}, (err, result) => {

		if(result !== null && result.username == request.body.username){
			return response.send("Duplicate User Found");
		} else {
			let newUser = new User({
				username: request.body.username,
				password: request.body.password
			});

			newUser.save((saveErr, savedUser) => {
				if(saveErr) {
					return console.error(saveErr)
				} else {
					return response.status(201).send("New user registered")
				}
			})

		}

	})
})

app.listen(port, () => console.log (`Server is running at port: ${port}`))